# The toolchain definition used to build the project.

CPP := clang++
CXX := clang++ -c
AR := ar
LD := clang++
LEX := flex
YACC := bison++
MKDIR := mkdir -p
RM := rm -rf
LN := ln -sf
CP := cp -rf

IGNORED_WARNINGS := c++98-compat \
		    c++98-compat-pedantic \
		    padded \
		    exit-time-destructors \
		    switch-enum

CPPFLAGS = --std=c++11 $(addprefix -I,$(INCLUDE_DIRS))
CXXFLAGS = $(CPPFLAGS) -Weverything $(addprefix -Wno-,$(IGNORED_WARNINGS))
ARFLAGS = cr
LDFLAGS = $(addprefix -l,$(LIBRARIES))
LFLAGS =
YFLAGS =

# Add flags for SDL using sdl-config.
CPPFLAGS += $(shell sdl-config --cflags)
LDFLAGS += $(shell sdl-config --libs)

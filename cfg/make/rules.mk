# This file defines the basic goals used to build the target file.

# Perform source file lookup in the sources directory.
vpath %.cpp $(SOURCES_DIR) $(AUTO_GENERATED_DIR)

# Define default goal.
.PHONY: default
.DEFAULT: default
default: $(DEFAULT_GOAL)

# Define the release and debug goals.
.PHONY: release debug
release debug: $(TARGET)

# Set the debug build's attributes.
debug: CXXFLAGS += -g -pedantic

# Set the release build's attributes.
release: CPPFLAGS += -DNDEBUG
release: CXXFLAGS += -O2 -Werror

# Compile object files from C++ sources. Dependency files are also created
# by this rule so that make will be able to include them for future builds.
$(BUILD_DIR)/%.o: %.cpp
	@$(MKDIR) $(@D)
	@$(CPP) -MM $(CPPFLAGS) -MP -MF $(@:%.o=%.dep) -MT $@ $<
	$(CXX) $(CXXFLAGS) -o$@ $<

# Define the rule to link the target file from all its' dependencies.
# The dependencies are defined in the main makefile.
$(TARGET):
	@$(MKDIR) $(@D)
	$(LD) $(LDFLAGS) -o$@ $(filter-out %.a,$^) $(filter %.a,$^)

# Define a target to generate the documentation.
.PHONY: doc
doc:
	doxygen $(CONFIGURATION_DIR)/doxygen.conf

# Cleanup goals, clean and purge.
.PHONY: clean
clean:
	-$(RM) $(BUILD_DIR)

.PHONY: purge
purge: clean
	-$(RM) $(AUTO_GENERATED_DIR)
	-$(RM) $(DOCUMENTATION_DIR)
ifneq ($(OS_NAME),windows)
	find . -type f -iname "*.log" -delete
	find . -type f -iname "*~" -delete
endif

# Mac-specific definitions.

APP_BUNDLE_CONTENTS_DIR := $(BUILD_DIR)/Bidoof.app/Contents
APP_BUNDLE_CONTENTS := MacOS/bidoof \
		       MacOS/res \
		       Resources/icon.icns \
		       Info.plist

# Define operating system value.
CPPFLAGS += -DMAC

# Set frameworks.
LDFLAGS += -framework Foundation \
           -framework CoreFoundation \
           -framework Cocoa

# SDLmain is required to compile on OS X.
LIBRARIES += SDLmain

# Create an .app bundle for OS X.
.PHONY: app_bundle
app_bundle: $(addprefix $(APP_BUNDLE_CONTENTS_DIR)/,$(APP_BUNDLE_CONTENTS))

$(APP_BUNDLE_CONTENTS_DIR)/MacOS/bidoof: $(TARGET)
$(APP_BUNDLE_CONTENTS_DIR)/MacOS/res: $(RESOURCES_DIR)

$(APP_BUNDLE_CONTENTS_DIR)/MacOS/%:
	@$(MKDIR) $(@D)
	$(LN) $(realpath $^) $@

$(APP_BUNDLE_CONTENTS_DIR)/Resources/%: $(RESOURCES_DIR)/mac/%
	@$(MKDIR) $(@D)
	$(CP) $(realpath $^) $@

$(APP_BUNDLE_CONTENTS_DIR)/%: $(RESOURCES_DIR)/mac/%
	@$(MKDIR) $(@D)
	$(CP) $(realpath $^) $@

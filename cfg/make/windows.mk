# Windows-specific definitions.

# Define operating system value.
CPPFLAGS += -DWINDOWS

# Fix target's extension.
TARGET := $(addsuffix .exe,$(TARGET))

# Toolchain fixes.
MKDIR := mkdir
RM := del /F /Q
LN := mklink /D
CP := copy /Y

# SDLmain is required to compile on Windows.
LIBRARIES += SDLmain

# Basic make definitions.

DEFAULT_GOAL := debug
SOURCES_DIR := src
AUTO_GENERATED_DIR := auto-generated
BUILD_DIR := bin
RESOURCES_DIR := res
CONFIGURATION_DIR := cfg
DOCUMENTATION_DIR := doc

# Build into separate directories under bin/.
ifeq ($(MAKECMDGOALS),)
  BUILD_DIR := $(BUILD_DIR)/$(DEFAULT_GOAL)
  AUTO_GENERATED_DIR := $(AUTO_GENERATED_DIR)/$(DEFAULT_GOAL)
else ifneq ($(filter $(MAKECMDGOALS),release debug),)
  BUILD_DIR := $(BUILD_DIR)/$(MAKECMDGOALS)
  AUTO_GENERATED_DIR := $(AUTO_GENERATED_DIR)/$(MAKECMDGOALS)
endif

# The main target.
TARGET := $(BUILD_DIR)/bidoof

# Source files are automatically gathered from modules' directories (supplied
# without the leading sources path). Object files' names are derived from the
# sources'. The MODULES variable is defined in the main makefile.
SOURCES = $(foreach module,$(MODULES),$(wildcard $(SOURCES_DIR)/$(module)/*.cpp))
OBJECTS = $(SOURCES:$(SOURCES_DIR)/%.cpp=$(BUILD_DIR)/%.o)

# Define include directories.
INCLUDE_DIRS := $(SOURCES_DIR) \
		$(AUTO_GENERATED_DIR) \
		$(SOURCES_DIR)/common

# Required libraries to link the executable.
# This list can be expanded in other makefiles using
#   LIBRARIES += <libraries>.
#
# The SDL library is added with sdl-config in LDFLAGS.
LIBRARIES := log4cpp \
	     boost_serialization \
	     SDL_image

# Determine operating system.
ifeq ($(OS),Windows_NT)
  OS_NAME := windows
else
  OS_NAME := $(shell uname -s)
  ifeq ($(OS_NAME),Darwin)
    OS_NAME := mac
  else ifeq ($(OS_NAME),Linux)
    OS_NAME := linux
  else
    $(error Could not determine operating system)
  endif
endif

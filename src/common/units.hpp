/*! @file units.hpp
 * @brief Units and dimensions definitions.
 */

#pragma once

#include <SDL/SDL_stdinc.h>

typedef Sint16 axis_t;
typedef Uint16 distance_t;

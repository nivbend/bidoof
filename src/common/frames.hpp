/*! @file frames.hpp
 * @brief Common frame-related definitions.
 */

#pragma once

#include <SDL/SDL_stdinc.h>

typedef Uint64 frame_t;

/*! @brief Frames per second. */
constexpr frame_t FPS = 60;

/*! @brief The intervals (in seconds) between frames. */
constexpr double FRAME_INTERVAL = (1000.0 / ((double)(FPS)));

#include "utility.hpp"

#include <SDL/SDL_video.h>
#include <SDL/SDL_image.h>
#include <string>

SDL_Surface *load_optimized(const std::string &file)
{
    SDL_Surface *const original = IMG_Load(file.c_str());

    if (nullptr == original)
    {
        return nullptr;
    }

    SDL_Surface *const optimized = SDL_DisplayFormatAlpha(original);

    /* Make a best effort and use the originl surface. */
    if (nullptr == optimized)
    {
        return original;
    }

    SDL_FreeSurface(original);

    return optimized;
}

#pragma once

#include <units.hpp>

#include <SDL/SDL_stdinc.h>
#include <boost/utility.hpp>

class Screen;

/*! @brief The base surface class.
 * @ingroup smeargle
 *
 * @details This class defines the basic API for classes which wrap SDL_Surface
 * instances, in some way or another.
 */
class Surface
    : public boost::noncopyable
{
public:
    virtual ~Surface();

    virtual bool is_valid() const = 0;
    virtual distance_t width() const = 0;
    virtual distance_t height() const = 0;

    /*! @brief Apply the surface on screen for drawing.
     * @param screen The Screen object on which to apply the surface.
     * @param x In world coordinates.
     * @param y In world coordinates.
     */
    virtual bool apply(Screen &screen, const axis_t x, const axis_t y) = 0;

protected:
    Surface();
};

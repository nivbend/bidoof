#pragma once

#include "Tile.hpp"
#include "tiling.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <boost/multi_array.hpp>
#include <boost/utility.hpp>
#include <string>
#include <memory>

class Image;

/*! @brief A collection of Tile objects which are clipped from the same image.
 * @ingroup smeargle
 *
 * @details The tiles are first generated in the constructor, and are stored in
 * a two-dimensional array, accessed either by their row and column numbers in
 * the original image, or by their index.
 * A tile's index is determined by its position in the image, from left to right
 * and top to bottom ('regular' row-major order).
 *
 * All tiles in the same tileset have a fixed width and height.
 */
class Tileset
    : public boost::noncopyable
{
public:
    typedef TileGrid<std::shared_ptr<Tile>> tiles_t;

public:
    Tileset(const std::string &file,
            const distance_t tile_width,
            const distance_t tile_height);

    virtual ~Tileset();

    /*! @{ @name Observers */
    grid_axis_t rows() const;
    grid_axis_t columns() const;
    distance_t tile_width() const;
    distance_t tile_height() const;
    /*! @} */

    /*! @{ @name %Tile Access */
    std::shared_ptr<Tile> tile(const grid_index_t index) const;
    std::shared_ptr<Tile> tile(const grid_axis_t row,
                               const grid_axis_t column) const;
    /*! @} */

private:
    const std::shared_ptr<Image> m_image;
    const distance_t m_tile_width;
    const distance_t m_tile_height;
    tiles_t m_tiles;
};

inline grid_axis_t Tileset::rows() const
{
    return get_rows(m_tiles);
}

inline grid_axis_t Tileset::columns() const
{
    return get_columns(m_tiles);
}

inline distance_t Tileset::tile_width() const
{
    return m_tile_width;
}

inline distance_t Tileset::tile_height() const
{
    return m_tile_height;
}

inline std::shared_ptr<Tile> Tileset::tile(const grid_index_t index) const
{
    return tile(
        static_cast<grid_axis_t>(index / columns()),
        static_cast<grid_axis_t>(index % columns()));
}

inline std::shared_ptr<Tile> Tileset::tile(const grid_axis_t row,
                                           const grid_axis_t column) const
{
    return m_tiles[row][column]->clone();
}

#include "Tile.hpp"
#include "Image.hpp"
#include "Screen.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <memory>

Tile::Tile(const std::shared_ptr<Image> &image,
           const axis_t x,
           const axis_t y,
           const distance_t width,
           const distance_t height)
: Surface()
, m_image(image)
, m_clip()
{
    m_clip.x = x;
    m_clip.y = y;
    m_clip.w = width;
    m_clip.h = height;
}

Tile::~Tile()
{} /* Do nothing. */

bool Tile::is_valid() const
{
    return m_image->is_valid();
}

/*! A tile which does not overlap the screen will not be drawn. */
bool Tile::apply(Screen &screen, const axis_t x, const axis_t y)
{
    /*! Avoid blitting off-screen imagery. */
    if (!(screen.camera().can_fit(x, y, width(), height())))
    {
        return true;
    }

    SDL_Rect position{0, 0, 0, 0};
    position.x = x;
    position.y = y;

    return screen.blit(m_image->surface(), &m_clip, &position);
}

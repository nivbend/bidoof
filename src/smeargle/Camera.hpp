#pragma once

#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <boost/utility.hpp>

/*! @brief Defines the view displayed on screen.
 * @ingroup smeargle
 *
 * @details The %Camera class defines the the screen coordinates system in
 * world- (map-) coordinates and is used to transform from the latter to the
 * former.
 */
class Camera final
    : public boost::noncopyable
{
public:
    Camera();

    virtual ~Camera();

    /*! @{ @name Observers */
    axis_t top() const;
    axis_t bottom() const;
    axis_t left() const;
    axis_t right() const;
    distance_t width() const;
    distance_t height() const;
    axis_t center_x() const;
    axis_t center_y() const;
    /*! @} */

    /*! @{ @name Mutators */
    /*! @brief Force-set the camera's position. */
    Camera &set_left(const axis_t x);
    Camera &set_top(const axis_t y);
    void set_center(const axis_t x, const axis_t y);

    /*! @brief Move the camera relative to its current position. */
    void move(const axis_t x, const axis_t y);
    /*! @} */

    /*! @brief Transform world coordinates to screen coordinates. */
    void adjust(axis_t &x, axis_t &y) const;

    /*! @{ @name Overlapping */
    /*! @{ */
    /*! @brief Check if an area fits on the screen. */
    bool can_fit(const SDL_Rect &area) const;
    bool can_fit(const axis_t x,
                 const axis_t y,
                 const distance_t width,
                 const distance_t height) const;
    /*! @} */

    /*! @{ */
    /*! @brief Check if the sides of an area fit horizontally on the screen. */
    bool can_fit_horizontally(const SDL_Rect &area) const;
    bool can_fit_horizontally(const axis_t x, const distance_t width) const;
    /*! @} */

    /*! @{ */
    /*! @brief Check if the sides of an area fit vertically on the screen. */
    bool can_fit_vertically(const SDL_Rect &area) const;
    bool can_fit_vertically(const axis_t y, const distance_t height) const;
    /*! @} */
    /*! @} */

private:
    SDL_Rect m_bounds;
};

inline axis_t Camera::top() const
{
    return m_bounds.y;
}

inline axis_t Camera::bottom() const
{
    return top() + ((axis_t)(height()));
}

inline axis_t Camera::left() const
{
    return m_bounds.x;
}

inline axis_t Camera::right() const
{
    return left() + ((axis_t)(width()));
}

inline distance_t Camera::width() const
{
    return m_bounds.w;
}

inline distance_t Camera::height() const
{
    return m_bounds.h;
}

inline axis_t Camera::center_x() const
{
    return left() + (width() / 2);
}

inline axis_t Camera::center_y() const
{
    return top() + (height() / 2);
}

inline bool Camera::can_fit(const SDL_Rect &area) const
{
    return can_fit(area.x, area.y, area.w, area.h);
}

inline bool Camera::can_fit_horizontally(const SDL_Rect &area) const
{
    return can_fit_horizontally(area.x, area.w);
}

inline bool Camera::can_fit_vertically(const SDL_Rect &area) const
{
    return can_fit_vertically(area.y, area.h);
}

/*! @file tiling.hpp
 * @brief Common tiling and tile-related definitions.
 */

#pragma once

#include <units.hpp>

#include <SDL/SDL_stdinc.h>
#include <boost/multi_array.hpp>

/*! @brief Grid's layout. */
enum Grid
{
    GRID_ROW, /*!< Rows' index in grid. */
    GRID_COLUMN, /*!< Columns' index in grid. */

    GRID_SIZE, /*!< Grid's dimension. */
};

/*! @brief Type of rows' and columns' numbers. */
typedef Uint16 grid_axis_t;

/*! @brief Type of the index in the grid.
 *
 * The index in the grid is the row-major index in the underlying
 * two-dimensional array of the grid.
 */
typedef Uint32 grid_index_t;

/*! @brief TEST? */
template <typename T>
using TileGrid = boost::multi_array<T, GRID_SIZE>;

/*! @brief Resolve number of rows from grid's shape. */
template <typename T>
inline grid_axis_t get_rows(const TileGrid<T> &tiles)
{
    return static_cast<grid_axis_t>(tiles.shape()[GRID_ROW]);
}

/*! @brief Resolve number of columns from grid's shape. */
template <typename T>
inline grid_axis_t get_columns(const TileGrid<T> &tiles)
{
    return static_cast<grid_axis_t>(tiles.shape()[GRID_COLUMN]);
}

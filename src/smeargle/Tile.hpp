#pragma once

#include "Surface.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <memory>

class Image;
class Screen;

/*! @brief A clipped section of a Tileset.
 * @ingroup smeargle
 */
class Tile final
    : public Surface
    , public std::enable_shared_from_this<Tile>
{
public:
    virtual ~Tile();

    Tile(const std::shared_ptr<Image> &image,
         const axis_t x,
         const axis_t y,
         const distance_t width,
         const distance_t height);

    bool is_valid() const override;
    distance_t width() const override;
    distance_t height() const override;

    bool apply(Screen &screen, const axis_t x, const axis_t y) override;

    std::shared_ptr<Tile> clone();

private:
    const std::shared_ptr<Image> m_image;
    SDL_Rect m_clip;
};

inline distance_t Tile::width() const
{
    return m_clip.w;
}

inline distance_t Tile::height() const
{
    return m_clip.h;
}

inline std::shared_ptr<Tile> Tile::clone()
{
    return shared_from_this();
}

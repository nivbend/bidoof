#pragma once

#include <SDL/SDL_video.h>
#include <string>

/*! @brief Load an optimized version of an image.
 * @ingroup smeargle
 */
SDL_Surface *load_optimized(const std::string &file);

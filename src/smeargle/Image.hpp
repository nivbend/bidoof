#pragma once

#include "Surface.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <memory>

/*! @brief A simple image surface.
 * @ingroup smeargle
 *
 * @details Encapsulates the very basic idea of a simple image loaded to a
 * surface, without any clipping or any other elaborate mechanisms.
 *
 * This class can be used, for example, to draw logos or other still images on
 * the screen.
 */
class Image
    : public Surface
{
private:
    typedef std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)> surface_t;

public:
    virtual ~Image();

    static Image *create(const std::string &image);

    bool is_valid() const override;
    distance_t width() const override;
    distance_t height() const override;
    SDL_Surface *surface() const;

    bool apply(Screen &screen, const axis_t x, const axis_t y) override;

protected:
    explicit Image(SDL_Surface *const surface);

private:
    surface_t m_surface;
};

inline bool Image::is_valid() const
{
    return (nullptr != surface());
}

inline SDL_Surface *Image::surface() const
{
    return m_surface.get();
}

inline distance_t Image::width() const
{
    return m_surface->clip_rect.w;
}

inline distance_t Image::height() const
{
    return m_surface->clip_rect.h;
}

#pragma once

#include "Camera.hpp"

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <boost/utility.hpp>
#include <memory>

class Surface;

/*! @brief The window's display.
 * @ingroup smeargle
 *
 * @details This class manages the surface on which the game's graphics will be
 * displayed.
 *
 * The screen's SDL_Surface instance is held as a raw pointer since SDL_Quit()
 * is in charge of deleting it when the program is finished (SDL_Quit() is
 * called by SceneManager::finalize()).
 *
 * The default background color is black.
 */
class Screen
    : public boost::noncopyable
{
public:
    Screen();

    virtual ~Screen();

    Camera &camera();
    const Camera &camera() const;

    /*! @brief Fill the screen with background color. */
    void clear();

    /*! @brief Apply the screen, updating the display. */
    void draw();

    /*!
     * @param source The surface to blit.
     * @param clip Clipping area.
     * @param position In world coordinates.
     */
    bool blit(SDL_Surface *const source,
              SDL_Rect *const clip,
              SDL_Rect *const position);

public:
    static constexpr distance_t WIDTH = 240;
    static constexpr distance_t HEIGHT = 160;
    static constexpr Uint8 BITS_PER_PIXEL = 32;

private:
    SDL_Surface *const m_surface;
    Camera m_camera;
    const Uint32 m_background_color;
};

inline Camera &Screen::camera()
{
    return m_camera;
}

inline const Camera &Screen::camera() const
{
    return m_camera;
}

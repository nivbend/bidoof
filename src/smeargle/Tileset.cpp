#include "Tileset.hpp"
#include "Image.hpp"
#include "Tile.hpp"
#include "tiling.hpp"
#include "utility.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <boost/multi_array.hpp>
#include <string>
#include <memory>

Tileset::Tileset(const std::string &file,
                 const distance_t tile_width,
                 const distance_t tile_height)
: m_image(Image::create(file))
, m_tile_width(tile_width)
, m_tile_height(tile_height)
, m_tiles()
{
    const grid_axis_t number_of_rows = m_image->height() / m_tile_height;
    const grid_axis_t number_of_columns = m_image->width() / m_tile_width;

    m_tiles.resize(boost::extents[number_of_rows][number_of_columns]);

    for (grid_axis_t row = 0; row < number_of_rows; ++row)
    {
        auto tile_row = m_tiles[row];

        for (grid_axis_t column = 0; column < number_of_columns; ++column)
        {
            tile_row[column].reset(new Tile(
                m_image,
                static_cast<axis_t>(column * m_tile_width),
                static_cast<axis_t>(row * m_tile_height),
                m_tile_width,
                m_tile_height));
        }
    }
}

Tileset::~Tileset()
{} /* Do nothing. */

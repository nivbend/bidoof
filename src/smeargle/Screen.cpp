#include "Screen.hpp"
#include "Surface.hpp"
#include <log/log.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>

Screen::Screen()
: m_surface(SDL_SetVideoMode(WIDTH, HEIGHT, BITS_PER_PIXEL, SDL_SWSURFACE))
, m_camera()
, m_background_color(SDL_MapRGB(m_surface->format, 0x00, 0x00, 0x00))
{
    if (nullptr == m_surface)
    {
        Log::fatal(Log::SMEARGLE)
            << "Error creating screen surface"
            << " (" << SDL_GetError() << ")";
    }
}

Screen::~Screen()
{} /* Do nothing. */

void Screen::clear()
{
    SDL_FillRect(m_surface, nullptr, m_background_color);
}

/*! This function calls SDL_Flip(). */
void Screen::draw()
{
    if (SDL_Flip(m_surface))
    {
        Log::error(Log::SMEARGLE)
            << "Error switching screen surfaces"
            << " (" << SDL_GetError() << ")";
    }
}

/*! First, the position is adjusted from world coordinates to the screen's,
 * then a call to SDL_BlitSurface() is made.
 */
bool Screen::blit(SDL_Surface *const source,
                  SDL_Rect *const clip,
                  SDL_Rect *const position)
{
    camera().adjust(position->x, position->y);

    if (SDL_BlitSurface(source, clip, m_surface, position))
    {
        Log::error(Log::SMEARGLE)
            << "Error copying sufraces"
            << " (" << SDL_GetError() << ")";

        return false;
    }

    return true;
}

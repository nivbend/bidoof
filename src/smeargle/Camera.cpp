#include "Camera.hpp"
#include "Screen.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>

Camera::Camera()
: m_bounds{0, 0, 0, 0}
{
    m_bounds.w = Screen::WIDTH;
    m_bounds.h = Screen::HEIGHT;
}

Camera::~Camera()
{} /* Do nothing. */

Camera &Camera::set_left(const axis_t x)
{
    m_bounds.x = x;

    return *this;
}

Camera &Camera::set_top(const axis_t y)
{
    m_bounds.y = y;

    return *this;
}

void Camera::set_center(const axis_t x, const axis_t y)
{
    set_left(x - (width() / 2));
    set_top(y - (height() / 2));
}

void Camera::move(const axis_t x, const axis_t y)
{
    m_bounds.x += x;
    m_bounds.y += y;
}

void Camera::adjust(axis_t &x, axis_t &y) const
{
    x -= left();
    y -= top();
}

bool Camera::can_fit(const axis_t x,
                     const axis_t y,
                     const distance_t width,
                     const distance_t height) const
{
    if (!(can_fit_horizontally(x, width)))
    {
        return false;
    }

    if (!(can_fit_vertically(y, height)))
    {
        return false;
    }

    return true;
}

bool Camera::can_fit_horizontally(const axis_t x, const distance_t width) const
{
    if (x + ((axis_t)(width)) < left())
    {
        return false;
    }

    if (right() < x)
    {
        return false;
    }

    return true;
}

bool Camera::can_fit_vertically(const axis_t y, const distance_t height) const
{
    /* In SDL screen coordinates, the Y value increases as it goes "down"
     * towards the bottom of the screen.
     */

    if ((y + ((axis_t)(height))) < top())
    {
        return false;
    }

    if (bottom() < y)
    {
        return false;
    }

    return true;
}

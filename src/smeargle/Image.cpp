#include "Image.hpp"
#include "Screen.hpp"
#include "utility.hpp"
#include <units.hpp>

#include <SDL/SDL_video.h>
#include <SDL/SDL_stdinc.h>
#include <assert.h>

Image::Image(SDL_Surface *const surface)
: Surface()
, m_surface(surface, &SDL_FreeSurface)
{
    assert(m_surface);
}

Image::~Image()
{} /* Do nothing. */

Image *Image::create(const std::string &image)
{
    SDL_Surface *const surface = load_optimized(image);

    return new Image(surface);
}

bool Image::apply(Screen &screen, const axis_t x, const axis_t y)
{
    /*! Avoid blitting off-screen imagery. */
    if (!(screen.camera().can_fit(x, y, width(), height())))
    {
        return true;
    }

    SDL_Rect position{0, 0, 0, 0};
    position.x = x;
    position.y = y;

    return screen.blit(m_surface.get(), nullptr, &position);
}

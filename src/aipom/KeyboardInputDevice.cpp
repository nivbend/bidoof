#include "KeyboardInputDevice.hpp"

#include <SDL/SDL_events.h>

KeyboardInputDevice::KeyboardInputDevice()
: InputDevice()
{} /* Do nothing. */

KeyboardInputDevice::~KeyboardInputDevice()
{} /* Do nothing. */

InputDevice::pointer_t KeyboardInputDevice::instance()
{
    static InputDevice::pointer_t instance(new KeyboardInputDevice());

    return instance;
}

void KeyboardInputDevice::process(const SDL_Event &event)
{
    switch (event.type)
    {
    case SDL_KEYDOWN:
        on_key_down(event.key);
        break;

    case SDL_KEYUP:
        on_key_up(event.key);
        break;

    default:
        break;
    }
}

void KeyboardInputDevice::on_key_down(const SDL_KeyboardEvent &event)
{
    switch (event.keysym.sym)
    {
    default:
        break;
    }
}

void KeyboardInputDevice::on_key_up(const SDL_KeyboardEvent &event)
{
    switch (event.keysym.sym)
    {
    default:
        break;
    }
}

#pragma once

#include "InputDevice.hpp"

#include <SDL/SDL_events.h>

/*! @brief An input device which ignoes all events.
 * @ingroup aipom
 *
 * @details This class implements the null-pointer idiom for InputDevice.
 */
class NullInputDevice final
    : public InputDevice
{
public:
    virtual ~NullInputDevice();

    static InputDevice::pointer_t instance();

    void process(const SDL_Event &event) override;

private:
    NullInputDevice();
};

inline void NullInputDevice::process(const SDL_Event &)
{} /* Do nothing. */

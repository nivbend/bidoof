#pragma once

#include <SDL/SDL_events.h>
#include <boost/utility.hpp>
#include <memory>

/*! @brief Input events enumeration
 * @ingroup aipom
 */
enum class Button
{
    INVALID = -1, /*!< Invalid event ID. */

    UP, /*!< Move up. */
    DOWN, /*!< Move down. */
    LEFT, /*!< Move left. */
    RIGHT, /*!< Move right. */
    A, /*!< @e A button pressed. */
    B, /*!< @e B button pressed. */
    START, /*!< @e Start button pressed. */
    SELECT, /*!< @e Select button pressed. */
};

/*! @brief Base input device
 * @ingroup aipom
 */
class InputDevice
    : public boost::noncopyable
{
public:
    typedef std::shared_ptr<InputDevice> pointer_t;

public:
    virtual ~InputDevice();

    /*! @brief Process an input event. */
    virtual void process(const SDL_Event &event) = 0;

protected:
    InputDevice();
};

#pragma once

#include "InputDevice.hpp"

#include <SDL/SDL_events.h>

/*! @brief Keyboard input device
 * @ingroup aipom
 *
 * @details This input device handles input from the keyboard.
 */
class KeyboardInputDevice final
    : public InputDevice
{
public:
    virtual ~KeyboardInputDevice();

    static InputDevice::pointer_t instance();

    void process(const SDL_Event &event) override;

private:
    /*! @brief Handle keys being pressed. */
    void on_key_down(const SDL_KeyboardEvent &event);

    /*! @brief Handle keys being released. */
    void on_key_up(const SDL_KeyboardEvent &event);

private:
    KeyboardInputDevice();
};

#include "NullInputDevice.hpp"

NullInputDevice::NullInputDevice()
: InputDevice()
{} /* Do nothing. */

NullInputDevice::~NullInputDevice()
{} /* Do nothing. */

InputDevice::pointer_t NullInputDevice::instance()
{
    static InputDevice::pointer_t instance(new NullInputDevice());

    return instance;
}

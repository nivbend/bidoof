#include <scenes/SceneManager.hpp>
#include <scenes/SplashScene.hpp>
#include <scenes/OverworldScene.hpp>
#include <log/log.hpp>

int main(void)
{
    SceneManager scene_manager;

    Log::initialize("res/log4cpp.properties");

    scene_manager.initialize();

    /* Load scenes in reverse order. */
    scene_manager.start_scene(new OverworldScene("res/sample.map"));
    scene_manager.push_scene(new SplashScene("res/logo.png", 120));

    while (scene_manager.is_running())
    {
        scene_manager.iterate();
    }

    scene_manager.finalize();

    return 0;
}

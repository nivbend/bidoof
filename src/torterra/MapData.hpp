#pragma once

#include "Map.hpp"

#include <boost/serialization/string.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/access.hpp>
#include <boost/utility.hpp>

#include <string>

/*! @brief Defines the file structure for Map serialization.
 * @ingroup torterra
 */
class MapData final
    : public boost::noncopyable
{
    friend class boost::serialization::access;

public:
    typedef Map::tiling_t tiling_t;

public:
    MapData();

    template <class Archive>
    void save(Archive &archive, const unsigned int) const;

    template <class Archive>
    void load(Archive &archive, const unsigned int);

    BOOST_SERIALIZATION_SPLIT_MEMBER()

public:
    std::string file;
    tiling_t tiling;
};

template <class Archive>
inline void MapData::save(Archive &archive, const unsigned int) const
{
    const grid_axis_t rows = get_rows(tiling);
    const grid_axis_t columns = get_columns(tiling);

    archive << file;
    archive << rows;
    archive << columns;

    for (auto row = tiling.begin(); row != tiling.end(); ++row)
    {
        for (auto tile = row->begin(); tile != row->end(); ++tile)
        {
            archive << *tile;
        }
    }
}

template <class Archive>
inline void MapData::load(Archive &archive, const unsigned int)
{
    grid_axis_t rows = 0;
    grid_axis_t columns = 0;

    archive >> file;
    archive >> rows;
    archive >> columns;

    tiling.resize(boost::extents[rows][columns]);

    for (distance_t row = 0; row < rows; ++row)
    {
        for (distance_t column = 0; column < columns; ++column)
        {
            archive >> tiling[row][column];
        }
    }
}

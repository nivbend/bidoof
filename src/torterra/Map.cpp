#include "Map.hpp"
#include "MapData.hpp"
#include <smeargle/Tileset.hpp>
#include <smeargle/Tile.hpp>
#include <smeargle/Screen.hpp>
#include <smeargle/tiling.hpp>
#include <log/log.hpp>
#include <units.hpp>

#include <SDL/SDL_stdinc.h>
#include <boost/archive/text_iarchive.hpp>
#include <fstream>
#include <string>
#include <memory>
#include <assert.h>

static auto get_extents(const Map::tiling_t &tiling)
    -> decltype(boost::extents[0][0])
{
    return boost::extents[get_rows(tiling)][get_columns(tiling)];
}

Map::Map(const Tileset &tileset, const tiling_t &tiling)
: Surface()
, m_tiles(get_extents(tiling))
{
    auto get_tile = [&](const tiling_t::element &tile_index)
    {
        return tileset.tile(tile_index);
    };

    std::transform(
        tiling.origin(),
        tiling.origin() + tiling.num_elements(),
        m_tiles.origin(),
        get_tile);
}

Map::~Map()
{} /* Do nothing. */

/*! Load a map from file using MapData. */
Map *Map::load(const std::string &file)
{
    typedef boost::archive::text_iarchive archive_t;

    std::ifstream input(file);
    archive_t archive(input);

    MapData data;
    archive >> data;

    Tileset tileset(data.file, TILE_WIDTH, TILE_HEIGHT);

    return new Map(tileset, data.tiling);
}

/*! Iterate over the tiling and apply each tile on the screen. Tiles which are
 * out of the screen's bounds will not be drawn.
 */
bool Map::apply(Screen &screen, const axis_t x, const axis_t y)
{
    unsigned int unssuccessful = 0;
    axis_t current_y = y;

    for (auto row = m_tiles.begin();
         row != m_tiles.end();
         ++row, current_y += ((axis_t)(TILE_HEIGHT)))
    {
        axis_t current_x = x;

        /* Check if row will fit on screen. */
        if (!(screen.camera().can_fit_vertically(current_y, TILE_HEIGHT)))
        {
            /* Current Y value will only increase past the bottom edge of the
             * screen, no point in iterating over the rest of the map.
             */
            if (screen.camera().bottom() < current_y)
            {
                break;
            }

            /* Skip rows until we reach screen's top edge. */
            continue;
        }

        for (auto tile = row->begin();
             tile != row->end();
             ++tile, current_x += ((axis_t)(TILE_WIDTH)))
        {
            /* Current X value will only increase past the right edge of the
             * screen, no point in iterating over the rest of the row.
             */
            if (screen.camera().right() < current_x)
            {
                break;
            }

            /* We make our best-effort to apply all tiles. */
            if (!((*tile)->apply(screen, current_x, current_y)))
            {
                ++unssuccessful;
            }
        }
    }

    if (0 < unssuccessful)
    {
        Log::error(Log::SMEARGLE)
            << "Failed to apply " << unssuccessful << " tiles.";

        return false;
    }

    return true;
}

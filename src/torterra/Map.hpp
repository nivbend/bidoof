#pragma once

#include <smeargle/Surface.hpp>
#include <smeargle/Tileset.hpp>
#include <smeargle/tiling.hpp>
#include <units.hpp>

#include <SDL/SDL_stdinc.h>
#include <boost/multi_array.hpp>
#include <string>
#include <memory>

class Screen;

/*! @brief A world map surface.
 * @ingroup torterra
 *
 * @details A world map which is drawn on screen using @ref Tileset "tilesets".
 *
 * A map consists of a Tileset object, and a tiling matrix which is holds the
 * index of each tile on the map.
 *
 * Maps are stored in <c>.map</c> files, using Boost's serialization library. The
 * structure of the <c>.map</c> file is defined in MapData.
 */
class Map
    : public Surface
{
public:
    typedef Tileset::tiles_t tiles_t;
    typedef TileGrid<grid_index_t> tiling_t;

public:
    virtual ~Map();

    static Map *load(const std::string &file);

    bool is_valid() const override;
    distance_t width() const override;
    distance_t height() const override;
    grid_axis_t rows() const;
    grid_axis_t columns() const;

    bool apply(Screen &screen, const axis_t x, const axis_t y) override;

protected:
    Map(const Tileset &tileset, const tiling_t &tiling);

public:
    static constexpr distance_t TILE_WIDTH = 16;
    static constexpr distance_t TILE_HEIGHT = 16;

private:
    tiles_t m_tiles;
};

inline bool Map::is_valid() const
{
    return true;
}

inline distance_t Map::width() const
{
    return columns() * TILE_WIDTH;
}

inline distance_t Map::height() const
{
    return rows() * TILE_HEIGHT;
}

inline grid_axis_t Map::rows() const
{
    return get_rows(m_tiles);
}

inline grid_axis_t Map::columns() const
{
    return get_columns(m_tiles);
}

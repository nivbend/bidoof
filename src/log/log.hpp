/*! @brief Logging interface
 *
 * Encapsulates the use <a href="http://log4cpp.sourceforge.net/">log4cpp</a>.
 *
 * There are five logging levels (in increasing importance):
 *   DEBUG, INFO, WARNING, ERROR and FATAL.
 * These partially correspond to log4cpp's Priority enumerations (the rest are
 * ignored):
 *   DEBUG, INFO, WARN, ERROR, FATAL.
 *
 * Logging is performed by simply writing to the log stream of the relevant
 * component:
 * @code
 *     Log::info(Log::MY_COMPONENT) << "Hello, World!";
 * @endcode
 *
 * Note that the components' log names should be explicitly and globally stated
 * in the Components header in this module by declaring in the log namespace:
 * @code
 *     constexpr const char *const MY_COMPONENT = "MY_COMPONENT";
 * @endcode
 *
 *
 * @see <a href="http://log4cpp.sourceforge.net/api/pages.html">log4cpp's API
 * documentation</a>
 */

#pragma once

#include <log4cpp/Category.hh>
#include <log4cpp/CategoryStream.hh>
#include <string>

namespace Log
{
    /*! @{ @name Modules' Component Names */
    constexpr const char *const SCENE_MANAGER = "SCENE_MANAGER";
    constexpr const char *const SCENE = "SCENE";
    constexpr const char *const SMEARGLE = "SMEARGLE";
    /*! @} */

    typedef log4cpp::Category logger;
    typedef log4cpp::CategoryStream stream;

    enum class level
    {
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        FATAL,
    };

    /* Initialize logging from configuration file. */
    void initialize(const std::string& configuration);

    stream log(const char *const componenet, const level level);
    stream debug(const char *const componenet);
    stream info(const char *const componenet);
    stream warning(const char *const componenet);
    stream error(const char *const componenet);
    stream fatal(const char *const componenet);
}

inline Log::stream Log::debug(const char *const componenet)
{
    return log(componenet, Log::level::DEBUG);
}

inline Log::stream Log::info(const char *const componenet)
{
    return log(componenet, Log::level::INFO);
}

inline Log::stream Log::warning(const char *const componenet)
{
    return log(componenet, Log::level::WARNING);
}

inline Log::stream Log::error(const char *const componenet)
{
    return log(componenet, Log::level::ERROR);
}

inline Log::stream Log::fatal(const char *const componenet)
{
    return log(componenet, Log::level::FATAL);
}

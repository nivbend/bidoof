#include "log.hpp"

#include <log4cpp/Category.hh>
#include <log4cpp/CategoryStream.hh>
#include <log4cpp/PropertyConfigurator.hh>
#include <string>

/*! Resolve log4cpp's logging stream from our log levels. */
static Log::stream resolve_stream(Log::logger& logger, const Log::level level)
{
    switch (level)
    {
    case Log::level::DEBUG:
        return logger.debugStream();

    case Log::level::INFO:
        return logger.infoStream();

    case Log::level::WARNING:
        return logger.warnStream();

    case Log::level::ERROR:
        return logger.errorStream();

    case Log::level::FATAL:
        return logger.fatalStream();
    }
}

static Log::logger &get_logger(const char *const componenet)
{
    if (nullptr == componenet)
    {
        /* We allow this case, but print an error. */
        resolve_stream(Log::logger::getRoot(), Log::level::ERROR)
            << "Logging out of any componenet";

        return Log::logger::getInstance("UNKNOWN");
    }

    return Log::logger::getInstance(componenet);
}

/*! Configure log4cpp from file. */
void Log::initialize(const std::string& configuration)
{
    log4cpp::PropertyConfigurator::configure(configuration);
}

Log::stream Log::log(const char *const componenet, const level level)
{
    return resolve_stream(get_logger(componenet), level);
}

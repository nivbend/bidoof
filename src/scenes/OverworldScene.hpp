#pragma once

#include "Scene.hpp"

#include <string>
#include <memory>

class Map;
class SceneManager;
class Screen;

/*! @brief Displays the overworld.
 * @ingroup scenes
 *
 * @details The overworld consists of the following components:
 * @li The underlying map.
 * @li Characters (PC or NPC).
 * @li Portals and other triggable entities.
 */
class OverworldScene final
    : public Scene
{
private:
    typedef std::unique_ptr<Map> map_t;

public:
    explicit OverworldScene(const std::string &map_file);

    virtual ~OverworldScene();

    void update(SceneManager &scene_manager) override;
    void render(Screen &screen) override;

    void initialize(SceneManager &scene_manager) override;
    void resume(SceneManager &scene_manager) override;
    void pause(SceneManager &scene_manager) override;

private:
    const map_t m_map;
};

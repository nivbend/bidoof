#pragma once

#include <boost/utility.hpp>
#include <string>
#include <memory>

class SceneManager;
class Screen;

/*! @brief Base scene class.
 * @ingroup scenes
 *
 * @details This class defines the basic scenes API, which is used (mostly by
 * the SceneManager) to control the life-cycle of scenes during runtime.
 */
class Scene
    : public boost::noncopyable
{
public:
    typedef std::unique_ptr<Scene> pointer_t;

public:
    virtual ~Scene();

    const std::string &name() const;
    bool is_finished() const;

    /*! @brief Update scene's state. */
    virtual void update(SceneManager &scene_manager) = 0;

    /*! @brief Render the scene on screen. */
    virtual void render(Screen &screen) = 0;

    /*! @brief Perform actions when first activating. */
    virtual void initialize(SceneManager &scene_manager);

    /*! @brief Perform actions when deactivating. */
    virtual void pause(SceneManager &scene_manager);

    /*! @brief Perform actions when activating. */
    virtual void resume(SceneManager &scene_manager);

    /*! @brief Perform pre-destruction actions. */
    virtual void destroy(SceneManager &scene_manager);

protected:
    explicit Scene(const std::string &name);

    void finish();

private:
    const std::string m_name;
    bool m_is_finished;
};

inline const std::string &Scene::name() const
{
    return m_name;
}

inline bool Scene::is_finished() const
{
    return m_is_finished;
}

inline void Scene::initialize(SceneManager &)
{} /* do nothing. */

inline void Scene::pause(SceneManager &)
{} /* do nothing. */

inline void Scene::resume(SceneManager &)
{} /* do nothing. */

inline void Scene::destroy(SceneManager &)
{} /* do nothing. */

inline void Scene::finish()
{
    m_is_finished = true;
}

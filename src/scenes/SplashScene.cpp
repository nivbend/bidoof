#include "SplashScene.hpp"
#include "SceneManager.hpp"
#include <smeargle/Screen.hpp>
#include <smeargle/Image.hpp>
#include <log/log.hpp>
#include <units.hpp>

#include <SDL/SDL_stdinc.h>
#include <string>

SplashScene::SplashScene(const std::string &image, const unsigned long duration)
: Scene("Splash")
, m_image(Image::create(image))
, m_x(static_cast<axis_t>(Screen::WIDTH - m_image->width()))
, m_y(static_cast<axis_t>(Screen::HEIGHT - m_image->height()))
, m_remaining(duration)
{
    if (!(m_image->is_valid()))
    {
        Log::error(Log::SCENE)
            << "Error loading image '" << image << "'"
            << " (" << SDL_GetError << ")";
    }
}

SplashScene::~SplashScene()
{} /* Do nothing. */

void SplashScene::update(SceneManager &)
{
    if (0 < m_remaining)
    {
        --m_remaining;

        return;
    }

    finish();
}

void SplashScene::render(Screen &screen)
{
    m_image->apply(screen, m_x, m_y);
}

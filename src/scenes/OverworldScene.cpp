#include "OverworldScene.hpp"
#include "SceneManager.hpp"
#include <torterra/Map.hpp>
#include <smeargle/Screen.hpp>
#include <aipom/KeyboardInputDevice.hpp>
#include <aipom/NullInputDevice.hpp>

#include <string>
#include <memory>

OverworldScene::OverworldScene(const std::string &map_file)
: Scene("Overworld")
, m_map(Map::load(map_file))
{} /* Do nothing. */

OverworldScene::~OverworldScene()
{} /* Do nothing. */

void OverworldScene::update(SceneManager &)
{} /* Do nothing. */

void OverworldScene::render(Screen &screen)
{
    m_map->apply(screen, 0, 0);
}

void OverworldScene::initialize(SceneManager &scene_manager)
{
    scene_manager.set_input_device(KeyboardInputDevice::instance());
}

void OverworldScene::resume(SceneManager &scene_manager)
{
    scene_manager.set_input_device(KeyboardInputDevice::instance());
}

void OverworldScene::pause(SceneManager &scene_manager)
{
    scene_manager.set_input_device(NullInputDevice::instance());
}

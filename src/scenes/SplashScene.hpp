#pragma once

#include "Scene.hpp"
#include <units.hpp>

#include <SDL/SDL_stdinc.h>
#include <string>
#include <memory>

class Image;
class SceneManager;
class Screen;

/*! @brief Splash screen scene.
 * @ingroup scenes
 *
 * @details Displays the splash screen while the game loads.
 *
 * Contradictory to what's expected, this scene should @e not be the first scene
 * (set by SceneManager::start_scene()). Since all this scene does is display
 * the splash screen for a given amount of time before declaring itself as
 * finished, setting it as the startup scene will cause the game to exit once
 * the splash screen vanishes.
 *
 * In order to display the splash screen, this scene should be pushed second,
 * right after the start scene is set. This way when this scene finishes, the
 * start scene will become the current scene.
 */
class SplashScene final
    : public Scene
{
public:
    SplashScene(const std::string &image, const unsigned long duration);

    virtual ~SplashScene();

    void update(SceneManager &scene_manager) override;
    void render(Screen &screen) override;

private:
    const std::unique_ptr<Image> m_image;
    const axis_t m_x;
    const axis_t m_y;
    unsigned long m_remaining;
};

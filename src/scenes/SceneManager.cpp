#include "SceneManager.hpp"
#include "Scene.hpp"
#include <smeargle/Screen.hpp>
#include <aipom/InputDevice.hpp>
#include <aipom/NullInputDevice.hpp>
#include <frames.hpp>

#include <SDL/SDL.h>
#include <assert.h>

frame_t SceneManager::s_frame_number = 0;

SceneManager::SceneManager()
: m_should_quit(false)
, m_scenes()
, m_screen(nullptr)
, m_input_device(NullInputDevice::instance())
{} /* Do nothing. */

SceneManager::~SceneManager()
{} /* Do nothing. */

bool SceneManager::is_running() const
{
    if (should_quit())
    {
        return false;
    }

    return true;
}

/*! This function calls SDL_Init() and creates the screen. */
void SceneManager::initialize()
{
    s_frame_number = 0;

    SDL_Init(SDL_INIT_EVERYTHING);

    m_screen.reset(new Screen());
}

void SceneManager::iterate()
{
    const ticks_t next_frame_time = static_cast<ticks_t>(SDL_GetTicks() + FRAME_INTERVAL);

    if (handle_input_events())
    {
        return;
    }

    if (update())
    {
        return;
    }

    draw();

    /* Delay before next frame. */
    const ticks_t end_time = SDL_GetTicks();
    if (end_time < next_frame_time)
    {
        SDL_Delay(next_frame_time - end_time);
    }

    ++s_frame_number;
}

void SceneManager::finalize()
{
    SDL_Quit();
}

void SceneManager::stop()
{
    m_should_quit = true;
}

void SceneManager::start_scene(Scene *const scene)
{
    assert(m_scenes.empty());

    m_scenes.push_back(scene);
    current_scene().initialize(*this);
}

/*! This function only pushes a @e new scene. To set the initial scene, call
 * start_scene().
 */
void SceneManager::push_scene(Scene *const scene)
{
    assert(!(m_scenes.empty()));

    current_scene().pause(*this);
    m_scenes.push_back(scene);
    current_scene().initialize(*this);
}

void SceneManager::set_input_device(const InputDevice::pointer_t &input)
{
    m_input_device = input;
}

bool SceneManager::should_quit() const
{
    if (m_scenes.empty())
    {
        return true;
    }

    return m_should_quit;
}

bool SceneManager::handle_input_events()
{
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            stop();
            break;

        default:
            m_input_device->process(event);
            break;
        }
    }

    return should_quit();
}

bool SceneManager::update()
{
    assert(!(m_scenes.empty()));

    Scene &scene = current_scene();

    scene.update(*this);

    if (scene.is_finished())
    {
        scene.destroy(*this);
        m_scenes.pop_back();
        current_scene().resume(*this);
    }

    return should_quit();
}

void SceneManager::draw()
{
    assert(!(m_scenes.empty()));

    m_screen->clear();
    current_scene().render(*m_screen);
    m_screen->draw();
}

#pragma once

#include <aipom/InputDevice.hpp>
#include <frames.hpp>

#include <SDL/SDL_stdinc.h>
#include <boost/utility.hpp>
#include <boost/ptr_container/ptr_deque.hpp>
#include <memory>

class Scene;
class Screen;

/*! @brief The game's run-time loop implementation.
 * @ingroup scenes
 *
 * @details An instance of this class manages the @ref Scene "scenes" and their
 * life-cycles, and is basically the run-time loop of the game.
 *
 * Scenes are stored in a stack, with the top being the currently active scene
 * (whose @ref Scene::update "update()" and @ref Scene::render "render()"
 * functions are called).
 *
 * A scene may push another scene to be the current scene, which will cause the
 * pushing scene's @ref Scene::pause "pause()" to be called, and the new scene's
 * @ref Scene::initialize "initialize()." When the new scene will finish, its
 * @ref Scene::destroy "destroy()" function will be called and the previous
 * scene's (the pushing scene) @ref Scene::resume "resume()" will be called
 * before the rendering stage.
 *
 * During each iteration, input events are handled, afterwhich the current
 * scene's @ref Scene::update "update()" and @ref Scene::render "render()"
 * functions are called.
 *
 * When a scene declares itself as finished (using Scene::set_finished()), it is
 * automatically popped form the stack. If the stack is empty, the game will
 * exit.
 */
class SceneManager final
    : public boost::noncopyable
{
private:
    typedef Uint32 ticks_t;
    typedef boost::ptr_deque<Scene> scenes_t;
    typedef std::unique_ptr<Screen> screen_t;

public:
    SceneManager();

    ~SceneManager();

    /*! @brief Perform setup operations needed to run the game. */
    void initialize();

    bool is_running() const;
    void iterate();

    /*! @brief Perform cleanup operations before exit. */
    void finalize();

    static frame_t current_frame();

    Screen &screen() const;

    void stop();

    /*! @brief Set the first scene. */
    void start_scene(Scene *const scene);

    /*! @brief Push a new scene on top of the current. */
    void push_scene(Scene *const scene);

    void set_input_device(const InputDevice::pointer_t &input);

private:
    bool should_quit() const;

    bool handle_input_events();
    bool update();
    void draw();

    Scene &current_scene();

private:
    static frame_t s_frame_number;

    bool m_should_quit;
    scenes_t m_scenes;
    screen_t m_screen;
    InputDevice::pointer_t m_input_device;
};

inline frame_t SceneManager::current_frame()
{
    return s_frame_number;
}

inline Scene &SceneManager::current_scene()
{
    return m_scenes.back();
}

inline Screen &SceneManager::screen() const
{
    return *m_screen;
}

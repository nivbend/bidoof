CONFIGURATION_DIR := cfg
MAKE_DIR := $(CONFIGURATION_DIR)/make

include $(MAKE_DIR)/definitions.mk
include $(MAKE_DIR)/toolchain.mk
include $(MAKE_DIR)/rules.mk
include $(MAKE_DIR)/$(OS_NAME).mk

MODULES := log \
           aipom \
           torterra \
           smeargle \
	   scenes \
	   main

# Include dependency files generated during compilation.
-include $(OBJECTS:%.o=%.dep)

# Define the target's dependency on all object files.
$(TARGET): $(OBJECTS)
